# VNC Protocol Handler for Windows
Allows the use of the *vnc://* URI scheme to launch a VNC Viewer instance on Windows.

Install realvnc
	- @powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
	- cinst realvnc -y

copy URLHelper.bat C:\Program Files\RealVNC\VNC Viewer\

RUN Register-Protocol.reg

Enjoy !!!